primus (0~20150328-13) unstable; urgency=medium

  [ Andreas Beckmann ]
  * Bump watch version to 4. No changes needed.
  * Bump Standards-Version to 4.5.1, no changes needed.

  [ Vincent Cheng ]
  * Remove myself from Uploaders.

 -- Andreas Beckmann <anbe@debian.org>  Thu, 07 Jan 2021 17:21:56 +0100

primus (0~20150328-12) unstable; urgency=medium

  * primus-nvidia: Build for arm64.

 -- Andreas Beckmann <anbe@debian.org>  Thu, 23 Jul 2020 23:15:02 +0200

primus (0~20150328-11) unstable; urgency=medium

  [ Andreas Beckmann ]
  * Add B-D: libglapi-mesa for seamless backports.
  * Switch to debhelper-compat (= 13).
  * Update Lintian overrides.
  * Recommend 'primus-libs:i386 (= ${binary:Version}) [amd64]' and get rid of
    primus-libs-ia32.
  * primus-libs: Recommend libprimus-vk1.
  * primus-nvidia: Recommend nvidia-primus-vk-wrapper.
  * primusrun: Export ENABLE_PRIMUS_LAYER=1 to let primus-vk (if available)
    handle Vulkan applications.
  * Build for more architectures.
  * Use setenv() instead of putenv() to avoid use-after-unload issues.
  * Work around setenv() issues in bash (see #962566).

  [ Debian Janitor ]
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository-
    Browse.
  * Rely on pre-initialized dpkg-architecture variables.

 -- Andreas Beckmann <anbe@debian.org>  Thu, 11 Jun 2020 21:11:23 +0200

primus (0~20150328-10) unstable; urgency=medium

  * Use upstream's primusrun and patch it for multiarch.  (Closes: #896140)
  * Move primus-libs-ia32 recommendation to primus-libs.
  * Set __GLVND_DISALLOW_PATCHING=1 before loading glvnd libraries.
  * Support glvnd variant of the nvidia driver.  (Closes: #951914, #946978)
  * Generate vendor-specific driver dependency lists for primus-nvidia.
  * Enable all hardening flags.
  * Remove PRIMUS_LOAD_GLOBAL HACK and link against libglapi.so.0 instead.
  * Set upstream metadata field: Repository.
  * Bump Standards-Version to 4.5.0, no changes needed.

 -- Andreas Beckmann <anbe@debian.org>  Mon, 09 Mar 2020 11:19:01 +0100

primus (0~20150328-9) unstable; urgency=medium

  * Fix building twice in a row.
  * Do not depend on transitional libgl1-mesa-glx package.
  * Upload to unstable.

 -- Andreas Beckmann <anbe@debian.org>  Sat, 24 Aug 2019 22:16:09 +0200

primus (0~20150328-8) experimental; urgency=medium

  * Add primus-nvidia metapackage.
  * Switch to debhelper-compat (= 12).
  * Bump Standards-Version to 4.4.0, no changes.
  * Add myself to Uploaders.
  * Upload to experimental.

 -- Andreas Beckmann <anbe@debian.org>  Thu, 01 Aug 2019 09:09:47 +0200

primus (0~20150328-7) unstable; urgency=medium

  [ Andreas Beckmann ]
  * Switch Vcs-* URLs to salsa.debian.org.
  * Set Rules-Requires-Root: no.
  * The dbgsym migration was done in stretch.
  * Extend the Breaks and Suggests to the NVIDIA 390xx legacy driver.

  [ Luca Boccassi ]
  * Switch to debhelper compat level 11.
  * Bump Standards-Version to 4.3.0, no changes.

 -- Luca Boccassi <bluca@debian.org>  Sat, 05 Jan 2019 11:29:10 +0100

primus (0~20150328-6) unstable; urgency=medium

  * Bump Standards-Version to 4.1.3, no changes.
  * Break: nvidia-driver-libs and suggest nvidia-driver-libs-nonglvnd.
    (Closes: #888020)

 -- Luca Boccassi <bluca@debian.org>  Mon, 22 Jan 2018 21:09:34 +0000

primus (0~20150328-5) unstable; urgency=medium

  * Switch to @debian.org email address.
  * Switch priority from "extra" to "optional" to comply with policy 4.0.1.
  * Use https for Vcs fields in d/control.
  * Use https for the d/copyright format field to comply with policy 4.0.0.
  * Bump Standards-Version to 4.1.1.
  * Do not install primus with the GLVND-enabled Nvidia packages, as they
    are not quite compatible yet. (Closes: #876033)

 -- Luca Boccassi <bluca@debian.org>  Tue, 10 Oct 2017 20:46:09 +0100

primus (0~20150328-4) unstable; urgency=medium

  * Depend alternatively on xserver-xorg-video-intel or
    xserver-xorg-core >= 2:1.18.3-2, as both enable SNA by default.

 -- Luca Boccassi <luca.boccassi@gmail.com>  Fri, 22 Jul 2016 08:37:15 +0100

primus (0~20150328-3) unstable; urgency=medium

  * Move xserver-xorg-video-intel dependency from primus-libs to primus.
    (Closes: #831640)

 -- Luca Boccassi <luca.boccassi@gmail.com>  Mon, 18 Jul 2016 09:30:57 +0100

primus (0~20150328-2) unstable; urgency=medium

  * Add myself to Uploaders.
  * Bump Standards-Version to 3.9.8, no changes.
  * Remove -dbg package in favour of -dbgsym.
  * Add dependency to xserver-xorg-video-intel >= 2.99.17 for SNA.

 -- Luca Boccassi <luca.boccassi@gmail.com>  Sun, 17 Jul 2016 18:28:11 +0100

primus (0~20150328-1) unstable; urgency=medium

  * New upstream git snapshot.
    - rebase on d1afbf6fce
    - refresh hardening.patch
  * Build with PRIMUS_UPLOAD=0 now that xserver-xorg-video-intel in sid has
    SNA enabled by default.
  * Add dependency on libgl1-mesa-glx to primus-libs. (LP: #1383909)
  * Move bumblebee bash-completion file from obsolete /etc/bash_completion.d/
    to /usr/share/bash-completion/completions/.

 -- Vincent Cheng <vcheng@debian.org>  Sat, 25 Jul 2015 20:42:42 -0700

primus (0~20140711-1) unstable; urgency=medium

  [ Vincent Cheng ]
  * New upstream git snapshot.
    - rebase on 4e000124ec
  * Update email address.
  * Update Standards version to 3.9.6, no changes required.

  [ Andreas Beckmann ]
  * hardening.patch: New. Use all variables that contain hardening flags.

 -- Vincent Cheng <vcheng@debian.org>  Wed, 15 Jan 2014 14:14:25 -0800

primus (0~20131127-2) unstable; urgency=medium

  * Build with PRIMUS_UPLOAD=1 to avoid a bug triggered by using primus and
    xserver-xorg-video-intel with UXA acceleration enabled (which is currently
    the default with xserver-xorg-video-intel 2:2.21.15-1 in unstable).
    This workaround will remain in effect until xserver-xorg-video-intel
    defaults to SNA in unstable. This flag will not be set if build
    environment is Ubuntu, as SNA is already enabled by default (in trusty).

 -- Vincent Cheng <Vincentc1208@gmail.com>  Tue, 07 Jan 2014 01:56:18 -0800

primus (0~20131127-1) unstable; urgency=medium

  * New upstream git snapshot.
    - rebase on 074817614c
  * Update Standards version to 3.9.5, no changes required.

 -- Vincent Cheng <Vincentc1208@gmail.com>  Mon, 06 Jan 2014 01:36:38 -0800

primus (0~20130904-1) unstable; urgency=low

  * New upstream git snapshot.
    - rebase on da6459236b

 -- Vincent Cheng <Vincentc1208@gmail.com>  Wed, 23 Oct 2013 01:03:15 -0700

primus (0~20130805-1) unstable; urgency=low

  * New upstream git snapshot.
    - rebase on 3f248d9f13

 -- Vincent Cheng <Vincentc1208@gmail.com>  Tue, 20 Aug 2013 02:13:51 -0700

primus (0~20130601-1) unstable; urgency=low

  * New upstream git snapshot.
    - rebase on 38731d5375
    - fixes FTBFS on oldstable with gcc < 4.6 (Closes: #710025)

 -- Vincent Cheng <Vincentc1208@gmail.com>  Thu, 06 Jun 2013 00:31:34 -0700

primus (0~20130527-1) unstable; urgency=low

  * New upstream git snapshot.
    - rebase on 618acb99e0

 -- Vincent Cheng <Vincentc1208@gmail.com>  Wed, 29 May 2013 01:49:06 -0700

primus (0~20130511-1) unstable; urgency=low

  * New upstream git snapshot.
    - rebase on ef73dd1976

 -- Vincent Cheng <Vincentc1208@gmail.com>  Thu, 16 May 2013 00:17:46 -0700

primus (0~20130427-1) unstable; urgency=low

  * Initial release. (Closes: #692597)

 -- Vincent Cheng <Vincentc1208@gmail.com>  Fri, 03 May 2013 03:26:17 -0700
